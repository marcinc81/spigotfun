import java.util.ArrayList;

public class Testy {

    public static void main(String[] args) {
        System.out.println("Zaczynamy.");

        ArrayList<Czlowiek> listaUczestnikow = new ArrayList<>();

        Czlowiek marcin = new Dyrekcja("Marcin");
        listaUczestnikow.add(marcin);

        Dyrekcja dyrektor = (Dyrekcja) marcin;

        Czlowiek gosia = new Uczen("Gosia");
        listaUczestnikow.add(gosia);

        Czlowiek zosia = new Uczen("Zosia");
        listaUczestnikow.add(zosia);


        for (Czlowiek uczestnik : listaUczestnikow) {
            System.out.println(uczestnik);
        }


        //marcin.krzycz();
    }

}


class Czlowiek {
    protected String imie;

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public Czlowiek(String imie) {
        this.imie = imie;
    }

    @Override
    public String toString() {
        return "Mam na imię " + imie;
    }
}


class Dyrekcja extends Czlowiek {

    public Dyrekcja(String imie) {
        super(imie);
    }

    public void krzycz() {
        System.out.println("Aaaaaaaa!");
    }
}


class Uczen extends Czlowiek {

    public Uczen(String imie) {
        super(imie);
    }

    public void rozrabiaj() {
        System.out.println("O jejku");
    }
}
