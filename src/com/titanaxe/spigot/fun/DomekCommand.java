package com.titanaxe.spigot.fun;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DomekCommand implements CommandExecutor {
    private World world;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        Bukkit.getConsoleSender().sendMessage("Serwer dzwoni po Boba Budowniczego... Chwileczkę!");

        if (sender instanceof Player) {
            Player player = (Player) sender;

            player.sendMessage(ChatColor.RED + "Postawię Ci tu śliczny domek! - Bob.");

            Location current = player.getLocation();
            world = current.getWorld();

            // przygotuj wolną przestrzeń do zbudowania domku
            clear(current, 20);

            // przygotuj teren
            ground(current, 20);

            // zbuduj domek
            house(current, 10);
        }

        return true;
    }


    /**
     * Czyści fragment mapy, tam gdzie wskazuje loc
     * @param loc
     * @param size
     */
    private void clear(Location loc, int size) {
        int cx = loc.getBlockX();
        int cy = loc.getBlockY();
        int cz = loc.getBlockZ();
        int f = size / 2;

        for (int x = cx - f; x < cx + f; x++) { // oś X
            for (int z = cz - f; z < cz + f; z++) { // oś Z
                for (int y = cy; y < cy + f; y++) { // oś Y
                    world.getBlockAt(x, y, z).setType(Material.AIR);
                }
            }
        }
    }


    /**
     * Przygotowuje teren pod domek
     * @param loc
     * @param size
     */
    private void ground(Location loc, int size) {
        int cx = loc.getBlockX();
        int cy = loc.getBlockY() - 1; // to co jest pod graczem
        int cz = loc.getBlockZ();
        int g = size / 2 - 1; // rozmiar gruntu
        int f = size / 2 - 2; // rozmiar fundamentu

        // upewnij sie ze tu da sie domek postawic
        for (int x = cx - g; x < cx + g; x++) { // oś X
            for (int z = cz - g; z < cz + g; z++) { // oś Z
                goodStand(x, cy, z);
            }
        }

        // zrob kamenny fundament
        for (int x = cx - f; x < cx + f; x++) { // oś X
            for (int z = cz - f; z < cz + f; z++) { // oś Z
                world.getBlockAt(x, cy, z).setType(Material.STONE);
            }
        }
    }


    /**
     * Tworzy podsypkę pod fundament, żeby dom nie stał w powietrzu
     */
    private void goodStand(int x, int cy, int z) {
        for (int y = cy; y > 0; y--) {
            Material material = world.getBlockAt(x, y, z).getType();
            if (!material.isSolid())
                world.getBlockAt(x, y, z).setType(Material.DIRT);
            else break;
        }

    }


    /**
     * Buduje piękny domek
     * @param loc
     */
    private void house(Location loc, int size) {
        int cx = loc.getBlockX();
        int cy = loc.getBlockY();
        int cz = loc.getBlockZ();
        int f = size / 2; // rozmiar domu
        int h = size / 3; // wysokosc sciany
        int wallHeight = 4;

        Material wallMaterial = Material.BRICK;
        Material windowMaterial = Material.GLASS;
        Material door = Material.IRON_DOOR;

        // ściany
        wall(cx - f, cy, cz - f, cx + f, cy + h, cz - f, wallMaterial); // wschód
        wall(cx - f, cy, cz - f, cx - f, cy + h, cz + f, wallMaterial); // połódnie
        wall(cx - f, cy, cz + f, cx + f, cy + h, cz + f, wallMaterial); // zachód
        wall(cx + f, cy, cz - f, cx + f, cy + h, cz + f, wallMaterial); // północ

        // okna
        wall(cx - 3, cy + 2, cz - f, cx - 2, cy + 2, cz - f, windowMaterial);
        wall(cx - 3, cy + 2, cz + f, cx - 2, cy + 2, cz + f, windowMaterial);

        // drzwi
        wall(cx + 1, cy, cz - f, cx + 1, cy + 1, cz - f, Material.AIR);

        // dach
        // ...
    }


    /**
     * Buduje ścianę
     */
    private void wall(int x1, int y1, int z1, int x2, int y2, int z2, Material material) {
        Bukkit.getConsoleSender().sendMessage(String.format("Bob stawia ścianę: (%d,%d,%d) -> (%d,%d,%d)", x1, y1, z1, x2, y2, z2));

        for (int x = x1; x <= x2; x++) {
            for (int z = z1; z <= z2; z++) {
                for (int y = y1; y <= y2; y++) {
                    world.getBlockAt(x, y, z).setType(material);
                }
            }
        }
    }
}
