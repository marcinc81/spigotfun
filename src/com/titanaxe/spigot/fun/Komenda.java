package com.titanaxe.spigot.fun;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Komenda implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        Bukkit.getConsoleSender().sendMessage("Witamy Pasjonatów!");

        if (sender instanceof Player) {
            Player player = (Player) sender;

            Location loc = player.getLocation();

            loc.add(0, -1, 0);
            loc.getBlock().setType(Material.WATER);
        }

        return true;
    }
}
