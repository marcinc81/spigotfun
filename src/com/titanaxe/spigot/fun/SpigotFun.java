package com.titanaxe.spigot.fun;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class SpigotFun extends JavaPlugin implements Listener {

    @Override
    public void onEnable(){
        this.getLogger().info("SpigotFun - enabled");

        this.getCommand("domek").setExecutor(new DomekCommand());
        this.getCommand("pasjonaci").setExecutor(new Komenda());

        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        this.getLogger().info("SpigotFun - disabled");
    }


    @EventHandler
    public void interact(PlayerInteractEvent ev) {
        // ...
    }

}
